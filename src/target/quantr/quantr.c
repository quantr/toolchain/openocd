#include <assert.h>
#include <stdlib.h>
#include <time.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <helper/bits.h>
#include <helper/log.h>
#include <helper/time_support.h>
#include <target/smp.h>

#include "jtag/jtag.h"
#include "quantr.h"
#include "rtos/rtos.h"
#include "target/algorithm.h"
#include "target/breakpoints.h"
#include "target/register.h"
#include "target/target.h"
#include "target/target_type.h"

COMMAND_HANDLER(handle_info) {
    LOG_DEBUG("");
    return 0;
}

static const struct command_registration quantr_exec_command_handlers[] = {
    {.name = "quantr",
     .handler = handle_info,
     .mode = COMMAND_EXEC,
     .usage = "",
     .help = "Displays some information OpenOCD detected about the Quantr."},
    COMMAND_REGISTRATION_DONE};

static int quantr_poll(struct target *target) {
    target->state = TARGET_HALTED;
    return ERROR_OK;
}

static int quantr_target_create(struct target *target, Jim_Interp *interp) {
    LOG_DEBUG("");
    struct quantr_common *quantr = calloc(1, sizeof(struct quantr_common));
    target->arch_info = quantr;
    return ERROR_OK;
}

static int quantr_init_target(struct command_context *cmd_ctx,
                              struct target *target) {
    LOG_DEBUG("");
    target->fileio_info = malloc(sizeof(*target->fileio_info));
    if (!target->fileio_info) {
        LOG_ERROR("out of memory");
        return ERROR_FAIL;
    }
    memset(target->fileio_info, 0, sizeof(*target->fileio_info));

    return ERROR_OK;
}

static int quantr_examine(struct target *target) {
    LOG_DEBUG("");
    int retval = ERROR_OK;
    return retval;
}

static int quantr_read_phys_memory(struct target *target, target_addr_t address,
                                   uint32_t size, uint32_t count,
                                   uint8_t *buffer) {
    LOG_DEBUG("");
    int retval = ERROR_COMMAND_SYNTAX_ERROR;
    return retval;
}

static int quantr_write_phys_memory(struct target *target,
                                    target_addr_t address, uint32_t size,
                                    uint32_t count, const uint8_t *buffer) {
    LOG_DEBUG("");
    int retval = ERROR_COMMAND_SYNTAX_ERROR;
    return retval;
}

int quantr_arch_state(struct target *target) {
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_halt(struct target *target) {
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_resume(struct target *target, int current,
                         target_addr_t address, int handle_breakpoints,
                         int debug_execution) {
    LOG_DEBUG("");
    struct breakpoint *bp;
    for (bp = target->breakpoints; bp; bp = bp->next) {
        LOG_DEBUG("bp->address=%x", bp->address);
    }
    return ERROR_OK;
}

static int quantr_step(struct target *target, int current,
                       target_addr_t address, int handle_breakpoints) {
    LOG_DEBUG("");
    return ERROR_OK;
}

const char *quantr_get_gdb_arch(struct target *target) { return "riscv:rv64"; }


static int register_get(struct reg *reg)
{
    LOG_DEBUG("");
    reg->value[0]=0xef;
    reg->value[1]=0xcd;
    reg->value[2]=0xab;
    reg->value[3]=0x89;
    reg->value[4]=0x67;
    reg->value[5]=0x45;
    reg->value[6]=0x23;
    reg->value[7]=0x01;
    //strncpy(reg->value, "abcd1234", 8);
	return ERROR_OK;
}

static int register_set(struct reg *reg, uint8_t *buf)
{
    LOG_DEBUG("");
    memcpy(reg->value, buf, DIV_ROUND_UP(reg->size, 8));
	return ERROR_OK;
}

static struct reg_arch_type quantr_reg_arch_type = {
	.get = register_get,
	.set = register_set
};

int quantr_get_gdb_reg_list(struct target *target, struct reg **reg_list[],
                            int *reg_list_size,
                            enum target_register_class reg_class) {
    *reg_list_size = 33;
    *reg_list = malloc(sizeof(struct reg *) * (*reg_list_size));
    struct reg **temp=(struct reg **)*reg_list;

    // LOG_DEBUG("pre temp[0] = %x", temp[0]);
    // LOG_DEBUG("pre temp[1] = %x", temp[1]);

    for (int x=0;x<33;x++){
        temp[x] = malloc(sizeof(struct reg));
        temp[x]->number=x;
        temp[x]->name=malloc(50);	
        sprintf(temp[x]->name, "peter reg %d", x);
        temp[x]->size = 64;
        temp[x]->exist = true;
        temp[x]->hidden = false;
        temp[x]->feature = malloc(sizeof(struct reg_feature));
        temp[x]->feature->name=malloc(10);
        strcpy(temp[x]->feature->name, "f1");
        temp[x]->reg_data_type = malloc(sizeof(struct reg_data_type));
        temp[x]->reg_data_type->type=REG_TYPE_INT64;
        temp[x]->reg_data_type->type_class=REG_TYPE_CLASS_VECTOR;
        temp[x]->reg_data_type->id=malloc(10);
        strcpy(temp[x]->reg_data_type->id, "0");
        temp[x]->value=malloc(10);
        *(temp[x]->value)=0x12345678012345678;
        temp[x]->group=NULL;
        temp[x]->type=malloc(sizeof(struct reg_arch_type));
        temp[x]->type=&quantr_reg_arch_type;
        temp[x]->arch_info=NULL;
        temp[x]->caller_save=false;
    }

	// LOG_DEBUG("qreg_list = %x", temp);
	// LOG_DEBUG("qreg_list[0] = %x", temp[0]);
	// LOG_DEBUG("qreg_list[1] = %x", temp[1]);
	// LOG_DEBUG("qreg_list_size = %d", *reg_list_size);		
	// LOG_DEBUG("qreg1[0]->feature = %x", temp[0]->feature);	
	// LOG_DEBUG("qreg1[0]->feature->name = %s", temp[0]->feature->name);	
	// LOG_DEBUG("qreg1[1]->feature = %x", temp[1]->feature);	
	// LOG_DEBUG("qreg1[1]->feature->name = %s", temp[1]->feature->name);	

    return ERROR_OK;
}

static unsigned quantr_xlen_nonconst(struct target *target)
{
	return 64;
}

static unsigned int quantr_data_bits(struct target *target)
{
	return 64;
} 

static int quantr_read_memory(struct target *target, target_addr_t address,
	uint32_t size, uint32_t count, uint8_t *buffer)
{
    LOG_DEBUG("");
    for (int x=0;x<size;x++){
        buffer[x]=x;
    }
	return ERROR_OK;
}

static int quantr_write_memory(struct target *target, target_addr_t address,
	uint32_t size, uint32_t count, const uint8_t *buffer)
{
    LOG_DEBUG("");
	return ERROR_OK;
}

static int quantr_add_breakpoint(struct target *target,
	struct breakpoint *breakpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_add_context_breakpoint(struct target *target,
	struct breakpoint *breakpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_add_hybrid_breakpoint(struct target *target,
	struct breakpoint *breakpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_remove_breakpoint(struct target *target, struct breakpoint *breakpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_add_watchpoint(struct target *target,
	struct watchpoint *watchpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_remove_watchpoint(struct target *target,
	struct watchpoint *watchpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

int quantr_hit_watchpoint(struct target *target,
	struct watchpoint **hit_watchpoint)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_assert_reset(struct target *target)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_deassert_reset(struct target *target)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_mmu(struct target *target, int *enabled)
{
    LOG_DEBUG("");
    return ERROR_OK;
}

static int quantr_virt2phys(struct target *target, target_addr_t virt,
			     target_addr_t *phys)
{
	return armv8_mmu_translate_va_pa(target, virt, phys, 1);
}

static int quantr_run_algorithm(struct target *target, int num_mem_params,
		struct mem_param *mem_params, int num_reg_params,
		struct reg_param *reg_params, target_addr_t entry_point,
		target_addr_t exit_point, int timeout_ms, void *arch_info)
{
    LOG_DEBUG("");
	return ERROR_OK;
}

struct target_type quantr_target = {
    .name = "quantr",

    .poll = quantr_poll,
    .arch_state = quantr_arch_state,

    .halt = quantr_halt,
    .resume = quantr_resume,
    .step = quantr_step,

	.assert_reset = quantr_assert_reset,
	.deassert_reset = quantr_deassert_reset,

    .target_create = quantr_target_create,
    .init_target = quantr_init_target,
    .examine = quantr_examine,

    .read_memory = quantr_read_memory,
	.write_memory = quantr_write_memory,

    .get_gdb_arch = quantr_get_gdb_arch,
    .get_gdb_reg_list = quantr_get_gdb_reg_list,
	.get_gdb_reg_list_noread = quantr_get_gdb_reg_list,

	.address_bits = quantr_xlen_nonconst,
	.data_bits = quantr_data_bits,

    .add_breakpoint = quantr_add_breakpoint,
	.remove_breakpoint = quantr_remove_breakpoint,
	// .add_context_breakpoint = quantr_add_context_breakpoint,
	// .add_hybrid_breakpoint = quantr_add_hybrid_breakpoint,
	.add_watchpoint = quantr_add_watchpoint,
	.remove_watchpoint = quantr_remove_watchpoint,
	// .hit_watchpoint = quantr_hit_watchpoint,

    .read_phys_memory = quantr_read_phys_memory,
    .write_phys_memory = quantr_write_phys_memory,
	.mmu = quantr_mmu,
	.virt2phys = quantr_virt2phys,

    .run_algorithm = quantr_run_algorithm,
};