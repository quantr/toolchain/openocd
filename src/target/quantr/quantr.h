#ifndef OPENOCD_TARGET_QUANTR_H
#define OPENOCD_TARGET_QUANTR_H

#include <jtag/jtag.h>

struct quantr_common {
	uint32_t stored_pc;
	int flush;
	bool debug_mode_enabled;
};

#endif
